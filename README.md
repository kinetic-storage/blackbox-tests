# blackbox-tests

A repository of black box tests for comparing across kinetic library implementations


# Kinetic Library Implementations

This repository tests a variety of kinetic library implementations:

* [Extensible C Client][repo-kinetic-prototype]
* [C++ Client][repo-kinetic-cpp]


<!-- Resources -->
[repo-kinetic-prototype]: https://gitlab.com/kinetic-storage/kinetic-prototype.git
[repo-kinetic-cpp]:       https://github.com/kinetic/kinetic-cpp-client.git
