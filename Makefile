TLD :=		$(shell pwd)

BUILDDIR    =	$(TLD)/build

# include and lib directories for kinetic-prototype
BUILDINC    =	$(BUILDDIR)/include
BUILDLIB    =	$(BUILDDIR)/lib

# include and lib directories for kinetic-cpp-client
CPPBUILDINC =	$(BUILDDIR)/cpp-include
CPPBUILDLIB =	$(BUILDDIR)/cpp-lib

# kinetic dependencies
KPROTOTYPEDIR =	$(TLD)/vendor/kinetic-prototype
KCPPCLIENTDIR =	$(TLD)/vendor/kinetic-cpp-client
KCCLIENTDIR   =	$(TLD)/vendor/kinetic-c-client
KPYCLIENTDIR  =	$(TLD)/vendor/kinetic-py-client

# Main programs and scripts
TBDIR         =	$(TLD)/toolbox
TESTDIR       =	$(TLD)/tests

# kinetic libraries
LKINETIC    =	$(BUILDLIB)/libkinetic.a
CPPLKINETIC =	$(CPPBUILDLIB)/libkinetic.a

CC      =	gcc
#CFLAGS  =	-g -I$(BUILDINC)
#CXXFLAGS =	-g -I$(CPPBUILDINC)
#LDFLAGS =	-L$(BUILDDIR)/lib

# ------------------------------
# Build Targets


all: $(BUILDDIR) $(LKINETIC) $(CPPLKINETIC) $(TBDIR) #$(TESTDIR)

test: $(TESTDIR)

$(BUILDDIR):
	@mkdir -p $(BUILDDIR)

$(TBDIR): FORCE
	(cd $@; BUILDDIR=$(BUILDDIR) make -e all install)

$(TESTDIR): FORCE
	(cd $@; BUILDDIR=$(BUILDDIR) make -e all install)


$(LKINETIC): FORCE
	@mkdir -p $(BUILDLIB) $(BUILDINC)
	# For some reason this build doesn't work, so for now it can be pre-built
	#(cd $(KPROTOTYPEDIR); BUILDDIR=$(BUILDDIR) make -e all install)
	/usr/bin/install -c -m 755 $(KPROTOTYPEDIR)/build/lib/libkinetic.a                        $(BUILDLIB)
	/usr/bin/install -c -m 755 $(KPROTOTYPEDIR)/build/lib/libgtest.a                          $(BUILDLIB)
	/usr/bin/install -c -m 755 $(KPROTOTYPEDIR)/build/lib/liblist.a                           $(BUILDLIB)
	/usr/bin/install -c -m 755 $(KPROTOTYPEDIR)/build/lib/libprotobuf-c.a                     $(BUILDLIB)
	@cp -rp $(KPROTOTYPEDIR)/build/include/* $(BUILDINC)
	# I tried using install assuming it was a better practice, but I'm not sure how to best preserve structure
	#/usr/bin/install -c -m 755 $(KPROTOTYPEDIR)/build/include/protobuf-c/protobuf-c.h         $(BUILDINC)
	#/usr/bin/install -c -m 755 $(KPROTOTYPEDIR)/build/include/kinetic/protocol/kinetic.pb-c.h $(BUILDINC)
	#/usr/bin/install -c -m 755 $(KPROTOTYPEDIR)/build/include/kinetic/kinetic.h               $(BUILDINC)
	#/usr/bin/install -c -m 755 $(KPROTOTYPEDIR)/build/include/kinetic/kinetic_types.h         $(BUILDINC)
	#/usr/bin/install -c -m 755 $(KPROTOTYPEDIR)/build/include/kinetic/protocol_types.h        $(BUILDINC)
	#/usr/bin/install -c -m 755 $(KPROTOTYPEDIR)/build/include/list.h                          $(BUILDINC)
	#/usr/bin/install -c -m 755 $(KPROTOTYPEDIR)/build/include/queue.h                         $(BUILDINC)
	#/usr/bin/install -c -m 755 $(KPROTOTYPEDIR)/build/include/stack.h                         $(BUILDINC)

# Uses a build directory, build-blackboxtest, during the build process
$(CPPLKINETIC): FORCE
	@mkdir -p $(KCPPCLIENTDIR)/build-blackboxtest $(CPPBUILDLIB) $(CPPBUILDINC)
	(cd $(KCPPCLIENTDIR)/build-blackboxtest; cmake $(KCPPCLIENTDIR); make)
	/usr/bin/install -c -m 755 $(KCPPCLIENTDIR)/build-blackboxtest/libkinetic_client_static.a              $(CPPBUILDLIB)
	@cp -rp $(KCPPCLIENTDIR)/include/* $(CPPBUILDINC)
	# I tried using install assuming it was a better practice, but I'm not sure how to best preserve structure
	#/usr/bin/install -c -m 755 $(KCPPCLIENTDIR)/include/kinetic/acls.h                                     $(CPPBUILDINC)
	#/usr/bin/install -c -m 755 $(KCPPCLIENTDIR)/include/kinetic/blocking_kinetic_connection.h              $(CPPBUILDINC)
	#/usr/bin/install -c -m 755 $(KCPPCLIENTDIR)/include/kinetic/blocking_kinetic_connection_interface.h    $(CPPBUILDINC)
	#/usr/bin/install -c -m 755 $(KCPPCLIENTDIR)/include/kinetic/byte_stream.h                              $(CPPBUILDINC)
	#/usr/bin/install -c -m 755 $(KCPPCLIENTDIR)/include/kinetic/common.h                                   $(CPPBUILDINC)
	#/usr/bin/install -c -m 755 $(KCPPCLIENTDIR)/include/kinetic/connection_options.h                       $(CPPBUILDINC)
	#/usr/bin/install -c -m 755 $(KCPPCLIENTDIR)/include/kinetic/drive_log.h                                $(CPPBUILDINC)
	#/usr/bin/install -c -m 755 $(KCPPCLIENTDIR)/include/kinetic/hmac_provider.h                            $(CPPBUILDINC)
	#/usr/bin/install -c -m 755 $(KCPPCLIENTDIR)/include/kinetic/incoming_value.h                           $(CPPBUILDINC)
	#/usr/bin/install -c -m 755 $(KCPPCLIENTDIR)/include/kinetic/key_range_iterator.h                       $(CPPBUILDINC)
	#/usr/bin/install -c -m 755 $(KCPPCLIENTDIR)/include/kinetic/kinetic.h                                  $(CPPBUILDINC)
	#/usr/bin/install -c -m 755 $(KCPPCLIENTDIR)/include/kinetic/kinetic_client.pb.h                        $(CPPBUILDINC)
	#/usr/bin/install -c -m 755 $(KCPPCLIENTDIR)/include/kinetic/kinetic_connection.h                       $(CPPBUILDINC)
	#/usr/bin/install -c -m 755 $(KCPPCLIENTDIR)/include/kinetic/kinetic_connection_factory.h               $(CPPBUILDINC)
	#/usr/bin/install -c -m 755 $(KCPPCLIENTDIR)/include/kinetic/kinetic_record.h                           $(CPPBUILDINC)
	#/usr/bin/install -c -m 755 $(KCPPCLIENTDIR)/include/kinetic/kinetic_status.h                           $(CPPBUILDINC)
	#/usr/bin/install -c -m 755 $(KCPPCLIENTDIR)/include/kinetic/message_stream.h                           $(CPPBUILDINC)
	#/usr/bin/install -c -m 755 $(KCPPCLIENTDIR)/include/kinetic/mock_incoming_value.h                      $(CPPBUILDINC)
	#/usr/bin/install -c -m 755 $(KCPPCLIENTDIR)/include/kinetic/mock_message_stream.h                      $(CPPBUILDINC)
	#/usr/bin/install -c -m 755 $(KCPPCLIENTDIR)/include/kinetic/nonblocking_kinetic_connection.h           $(CPPBUILDINC)
	#/usr/bin/install -c -m 755 $(KCPPCLIENTDIR)/include/kinetic/nonblocking_kinetic_connection_interface.h $(CPPBUILDINC)
	#/usr/bin/install -c -m 755 $(KCPPCLIENTDIR)/include/kinetic/nonblocking_packet_service_interface.h     $(CPPBUILDINC)
	#/usr/bin/install -c -m 755 $(KCPPCLIENTDIR)/include/kinetic/outgoing_value.h                           $(CPPBUILDINC)
	#/usr/bin/install -c -m 755 $(KCPPCLIENTDIR)/include/kinetic/reader_writer.h                            $(CPPBUILDINC)
	#/usr/bin/install -c -m 755 $(KCPPCLIENTDIR)/include/kinetic/status.h                                   $(CPPBUILDINC)
	#/usr/bin/install -c -m 755 $(KCPPCLIENTDIR)/include/kinetic/status_code.h                              $(CPPBUILDINC)
	#/usr/bin/install -c -m 755 $(KCPPCLIENTDIR)/include/kinetic/threadsafe_blocking_kinetic_connection.h   $(CPPBUILDINC)
	#/usr/bin/install -c -m 755 $(KCPPCLIENTDIR)/include/kinetic/threadsafe_nonblocking_connection.h        $(CPPBUILDINC)


# ------------------------------
# Convenience Targets
lkinetic: $(LKINETIC)

cpplkinetic: $(CPPLKINETIC)

toolbox: $(TBDIR)


# ------------------------------
# Cleaning targets

clean: kineticclean kinetic-cpp-clean toolboxclean #testclean
	rm -rf $(BUILDDIR)

kineticclean:
	rm -rf $(BUILDINC) $(BUILDLIB)

kinetic-cpp-clean:
	rm -rf $(CPPBUILDINC) $(CPPBUILDLIB)

toolboxclean:
	(cd $(TBDIR); make clean)

testclean:
	(cd $(TESTDIR); make clean)

distclean:
	#(cd $(TBDIR); make clean)
	#(cd $(TESTDIR); make clean)
	rm -rf $(BUILDDIR)

.PHONY: FORCE
FORCE:

