#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>

#include <glog/logging.h>
#include <kinetic/kinetic.h>
#include "kctl.h"
#include "gflags/gflags.h"

using com::seagate::kinetic::client::proto::Command_Algorithm_SHA1;
using kinetic::Status;
using kinetic::KineticStatus;
using kinetic::KineticRecord;

using std::shared_ptr;
using std::string;
using std::unique_ptr;

#define MAXTAG 32

#define CMD_USAGE(_ka) kctl_put_usage(_ka)
int
kctl_put_usage(struct kargs *ka)
{
        fprintf(stderr, "Usage: %s [..] %s [CMD OPTIONS] KEY VALUE\n",
		ka->ka_progname, ka->ka_cmdstr);
	fprintf(stderr, "\nWhere, CMD OPTIONS are [default]:\n");
	fprintf(stderr, "\t-c           Compare and swap [no]\n");
	fprintf(stderr, "\t-p [wt|wb|f] Persist Mode: writethrough, writeback, \n");
	fprintf(stderr, "\t             flush [writeback]\n");
	fprintf(stderr, "\t-t tag       Tag key with an arbritrary str [""] \n");
	fprintf(stderr, "\t             limited to %d characters\n", MAXTAG);
	fprintf(stderr, "\t-?           Help\n");
	fprintf(stderr, "\nWhere, KEY and VALUE are quoted strings that can contain arbitrary\n");
	fprintf(stderr, "hexidecimal escape sequences to encode binary characters.\n");
	fprintf(stderr, R"foo(Only \xHH escape sequences are converted, ex \xF8.)foo");
	fprintf(stderr, "\nIf a conversion fails the command terminates.\n");

	fprintf(stderr, "\nTo see available COMMON OPTIONS: ./kctl -?\n");
}

// Put a Key Value pair
// By default the version number is stored as 8 digit hexidecimal number
// starting at 0 for new keys.
// By default versions are ignored, compare and swap can be enabled with -c
//    This enforces the correct version is passed to the put or else it fails
// All persistence modes are supported with -p [wt,wb,f] defaulting to WRITE_BACK
// Tags can be set with -t tag
int
kctl_put(int argc, char *argv[],
	 unique_ptr<KCTL_CONTYPE>& kcon,
	 struct kargs *ka)
{
 	extern char     	*optarg;
        extern int		optind, opterr, optopt;
        char			c;
	kinetic::PersistMode	pmode = kinetic::PersistMode::WRITE_BACK;
	kinetic::WriteMode	wmode = kinetic::WriteMode::IGNORE_VERSION;
	int 			cmpswp=0, exists=0;
	char			*tag = strdup("");
	char			nversion[11]; 	// holds hex representation of
						// one int: "0x00000000"

        while ((c = getopt(argc, argv, "ch?p:")) != EOF) {
                switch (c) {
		case 'c':
			cmpswp = 1;
			wmode = kinetic::WriteMode::REQUIRE_SAME_VERSION;
			break;
		case 'p':
			if (strlen(optarg) > 2) {
				fprintf(stderr, "**** Bad -p flag option %s\n",
					optarg);
				CMD_USAGE(ka);
				return(-1);
			}
			if (strncmp(optarg, "wt", 2) == 0)
				pmode = kinetic::PersistMode::WRITE_THROUGH;
			else if (strncmp(optarg, "wb", 2) == 0)
				pmode = kinetic::PersistMode::WRITE_BACK;
			else if (strncmp(optarg, "f", 1) == 0)
				pmode = kinetic::PersistMode::FLUSH;
			else {
				fprintf(stderr, "**** Bad -p flag option: %s\n",
					optarg);
				CMD_USAGE(ka);
				return(-1);
			}
			break;
		case 't':
			tag = strndup(optarg, MAXTAG);
		case 'h':
                case '?':
                default:
                        CMD_USAGE(ka);
			return(-1);
		}
        }

	// Check for the key and value parms
	if (argc - optind == 2) {
		if (!asciidecode(argv[optind], strlen(argv[optind]),
				 (void **)&ka->ka_key, &ka->ka_keylen)) {
			fprintf(stderr, "*** Failed key conversion\n");
			CMD_USAGE(ka);
			return(-1);
		}
		optind++;
		if (!asciidecode(argv[optind], strlen(argv[optind]),
				 (void **)&ka->ka_val, &ka->ka_vallen)) {
			fprintf(stderr, "*** Failed key conversion\n");
			CMD_USAGE(ka);
			return(-1);
		}
		
	} else {
		fprintf(stderr, "*** Too few or too many args\n");
		CMD_USAGE(ka);
		return(-1);
	}

	std::unique_ptr<string> version;
	std::string key(ka->ka_key, ka->ka_keylen);
	std::string value(ka->ka_val, ka->ka_vallen);

	// Get the key's version if it exists and then increment the version
	KineticStatus kstatus = kcon->GetVersion(ka->ka_key, version);
	if(kstatus.ok()) {
		unsigned long nv;
		nv = strtoul(version->c_str(), NULL, 16) + 1;
		sprintf(nversion, "0x%08x", (unsigned int)nv);
		exists = 1;
	} else { 
		// Get failure likely means no existing key so no version
		// use default 0
		sprintf(nversion, "0x%08x", 0);
	}
	
	const kinetic::KineticRecord nrecord(value, nversion, tag,
					    Command_Algorithm_SHA1);
	if (ka->ka_verbose) {
		printf("Compare & Swap:  %s\n", cmpswp?"Enabled":"Disabled");
		printf("Tag:             %s\n", tag);
		printf("Current Version: %s\n", exists?version->c_str():"");
		printf("New Version:     %s\n", nrecord.version()->c_str());
	}
	
        if (exists)
		kstatus = kcon->Put(key, version->c_str(), wmode, nrecord, pmode);
	else
		kstatus = kcon->Put(key, "", wmode, nrecord, pmode);
	
	if(!kstatus.ok()) {
		fprintf(stderr, "%s: Unable to put key: %s\n",
			ka->ka_cmdstr, kstatus.message().c_str());
		return(-1);
        }	
	return(0);
}


