#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>

#include <glog/logging.h>
#include <kinetic/kinetic.h>
#include "kctl.h"
#include "gflags/gflags.h"

using com::seagate::kinetic::client::proto::Command_Algorithm_SHA1;
using kinetic::Status;
using kinetic::KineticStatus;
using kinetic::KineticRecord;

using std::shared_ptr;
using std::string;
using std::unique_ptr;

#define CMD_USAGE(_ka) kctl_cluster_usage(_ka)
int
kctl_cluster_usage(struct kargs *ka)
{
        fprintf(stderr, "Usage: %s [..] %s [CMD OPTIONS] -c VERSION\n",
		ka->ka_progname, ka->ka_cmdstr);
	fprintf(stderr, "\nWhere, CMD OPTIONS are [default]:\n");
	fprintf(stderr, "\t-c VERSION   New Cluster Version\n");
	fprintf(stderr, "\t-?           Help\n");
	fprintf(stderr, "\nWhere, VERSION is a decimal number\n");
	fprintf(stderr, "\nTo see available COMMON OPTIONS: ./kctl -?\n");
}

// Set the cluster version at the kinetic server
int
kctl_cluster(int argc, char *argv[],
	 unique_ptr<KCTL_CONTYPE>& kcon,
	 struct kargs *ka)
{
 	extern char     	*optarg;
        extern int		optind, opterr, optopt;
        char			c, *cp;
	int64_t			cversion = -1;

        while ((c = getopt(argc, argv, "c:h?")) != EOF) {
                switch (c) {
		case 'c':
			cversion = strtol(optarg, &cp, 0);
			if (!cp || *cp != '\0') {
				fprintf(stderr, "*** Invalid Cluster Version %s\n",
				       optarg);
				CMD_USAGE(ka);
				return(-1);
			}
			break;
		case 'h':
                case '?':
                default:
                        CMD_USAGE(ka);
			return(-1);
		}
        }

	// cversion not set
	if (cversion == -1) {
		fprintf(stderr, "*** -c VERSION is required\n");
		CMD_USAGE(ka);
		return(-1);
	}
	   
	// Check for erroneous params
	if (argc - optind > 0) {
		fprintf(stderr, "*** Too many args\n");
		CMD_USAGE(ka);
		return(-1);
	}

	// Set the current cluster version atr the server
	KineticStatus kstatus = kcon->SetClusterVersion(cversion);

	if(!kstatus.ok()) {
		fprintf(stderr, "%s: Unable to set server cluster version: %s\n",
			ka->ka_cmdstr, kstatus.message().c_str());
		return(-1);
	}		
	
	return(0);
}
