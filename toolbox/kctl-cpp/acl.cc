#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>

#include <glog/logging.h>
#include <kinetic/kinetic.h>
#include "kctl.h"
#include "gflags/gflags.h"

using com::seagate::kinetic::client::proto::Command_Algorithm_SHA1;
using kinetic::Status;
using kinetic::KineticStatus;
using kinetic::KineticRecord;
using kinetic::ACL;
using kinetic::Scope;

using std::string;
using std::list;
using std::make_shared;
using std::shared_ptr;
using std::unique_ptr;

#define CMD_USAGE(_ka) kctl_acl_usage(_ka)
int
kctl_acl_usage(struct kargs *ka)
{
        fprintf(stderr, "Usage: %s [..] %s [CMD OPTIONS] -u id -m hmac [PERMS]\n",
		ka->ka_progname, ka->ka_cmdstr);
	fprintf(stderr, "\nWhere, these and at least 1 PERM are required:\n");
	fprintf(stderr, "\t-u id        New or existing User ID (number)\n");
	fprintf(stderr,	"\t-m hmac      New or existing HMAC Key (string)\n");
	fprintf(stderr, "\nWhere, PERMS can be any combination of:\n");
	fprintf(stderr, "\t-a           Set all Permissions\n");
	fprintf(stderr, "\t-d           Delete Op Permission\n");
	fprintf(stderr, "\t-g           GetLog Op Permission\n");
	fprintf(stderr, "\t-r           Read Op Permission\n");
	fprintf(stderr, "\t-R           Range Op Permission\n");
	fprintf(stderr, "\t-w           Write Op Permission\n");
	fprintf(stderr, "\t-p           Peer 2 Peer Op Permission\n");
	fprintf(stderr, "\t-s           Setup Op Permission\n");
	fprintf(stderr, "\t-S           Security Op Permission\n");
	fprintf(stderr, "\nWhere, CMD OPTIONS are [default]:\n");
	fprintf(stderr, "\t-?           Help\n");
	fprintf(stderr, "\nTo see available COMMON OPTIONS: ./kctl -?\n");
}

// Set the user requested ACL
int
kctl_acl(int argc, char *argv[],
	 unique_ptr<KCTL_CONTYPE>& kcon,
	 struct kargs *ka)
{
 	extern char     	*optarg;
        extern int		optind, opterr, optopt;
        char			c, *cp, all = 0;
	Scope scope = {.offset = 0, .value = "", .permissions={}};
	std::list<Scope> scopes;
	ACL acl;
	acl.identity=-1;
	acl.hmac_key = "";
	auto acls = make_shared<list<ACL>>();
	
        while ((c = getopt(argc, argv, "u:m:adgrRwpsSh?")) != EOF) {
                switch (c) {
		case 'u':
			acl.identity = strtol(optarg, &cp, 0);
			if (!cp || *cp != '\0') {
				fprintf(stderr, "*** Invalid ID %s\n",
				       optarg);
				CMD_USAGE(ka);
				return(-1);
			}
			break;
			
		case 'a':
			all = 1;
			break;
			
		case 'm':
			acl.hmac_key = optarg;
			break;
			
		case 'd':
			scope.permissions.push_front(kinetic::DELETE);
			break;
			
		case 'g':
			scope.permissions.push_front(kinetic::GETLOG);
			break;
			
		case 'r':
			scope.permissions.push_front(kinetic::READ);
			break;
			
		case 'R':
			scope.permissions.push_front(kinetic::RANGE);
			break;
			
		case 'w':
			scope.permissions.push_front(kinetic::WRITE);
			break;
			
		case 'p':
			scope.permissions.push_front(kinetic::P2POP);
			break;
			
		case 's':
			scope.permissions.push_front(kinetic::SETUP);
			break;
			
		case 'S':
			scope.permissions.push_front(kinetic::SECURITY);
			break;
			
		case 'h':
                case '?':
                default:
                        CMD_USAGE(ka);
			return(-1);
		}
        }


	if (acl.identity == -1) {
		fprintf(stderr, "*** -u ID is required\n");
		CMD_USAGE(ka);
		return(-1);
	}

	if (acl.hmac_key.empty()) {
		fprintf(stderr, "*** -m HMAC ID is required\n");
		CMD_USAGE(ka);
		return(-1);
	}
	
	if (scope.permissions.empty()) {
		fprintf(stderr, "*** at least 1 PERM is required\n");
		CMD_USAGE(ka);
		return(-1);
	}
	   
	// Check for erroneous params
	if (argc - optind > 0) {
		fprintf(stderr, "*** Too many args\n");
		CMD_USAGE(ka);
		return(-1);
	}

	if (all) {
		// because we could get -a with other perm flags,
		// need to clear the list and then push all perms,
		// dont want dups
		scope.permissions.clear();
		scope.permissions.push_front(kinetic::DELETE);
		scope.permissions.push_front(kinetic::GETLOG);
		scope.permissions.push_front(kinetic::READ);
		scope.permissions.push_front(kinetic::RANGE);
		scope.permissions.push_front(kinetic::WRITE);
		scope.permissions.push_front(kinetic::P2POP);
		scope.permissions.push_front(kinetic::SETUP);
		scope.permissions.push_front(kinetic::SECURITY);
	}
	
	// setup the scopes and acls now that user data is there
	scopes.push_front(scope);
	acl.scopes = scopes;
	acls->push_front(acl);

	// Set the ACLs based on the user request
	KineticStatus kstatus = kcon->SetACLs(acls);

	if(!kstatus.ok()) {
		fprintf(stderr, "%s: Unable to set ACLs: %s\n",
			ka->ka_cmdstr, kstatus.message().c_str());
		return(-1);
	}		
	
	return(0);
}
