#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>

#include <glog/logging.h>
#include <kinetic/kinetic.h>
#include "kctl.h"
#include "gflags/gflags.h"

using com::seagate::kinetic::client::proto::Command_Algorithm_SHA1;
using kinetic::Status;
using kinetic::KineticStatus;
using kinetic::KineticRecord;

using std::shared_ptr;
using std::string;
using std::unique_ptr;

#define CMD_USAGE(_ka) kctl_ping_usage(_ka)

int
kctl_ping_usage(struct kargs *ka)
{
        fprintf(stderr, "Usage: %s [..] %s [CMD OPTIONS]\n",
		ka->ka_progname, ka->ka_cmdstr);
	fprintf(stderr, "\nWhere, CMD OPTIONS are [default]:\n");
	fprintf(stderr, "\t-?           Help\n");
	fprintf(stderr, "\nTo see available COMMON OPTIONS: ./kctl -?\n");
}

// Ping the kinetic server
int
kctl_ping(int argc, char *argv[],
	 unique_ptr<KCTL_CONTYPE>& kcon,
	 struct kargs *ka)
{
 	extern char     	*optarg;
        extern int		optind, opterr, optopt;
        char			c;

        while ((c = getopt(argc, argv, "h?")) != EOF) {
                switch (c) {
		case 'h':
                case '?':
                default:
                        CMD_USAGE(ka);
			return(-1);
		}
        }

	// Check for erroneous params
	if (argc - optind > 0) {
		fprintf(stderr, "*** Too many args\n");
		CMD_USAGE(ka);
		return(-1);
	}

	// A noop hits the server and return informational status
	KineticStatus kstatus = kcon->NoOp();
	if(!kstatus.ok()) {
		fprintf(stderr, "%s: Unable to ping server: %s\n",
			ka->ka_cmdstr, kstatus.message().c_str());
		return(-1);
	}		
	
	// Show the cluster version Number
	if (ka->ka_verbose)
		printf("Cluster Version: %ld\n",
		       kstatus.expected_cluster_version());

	return(0);
}
