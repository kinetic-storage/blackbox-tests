#ifndef _KCTL_H
#define _KCTL_H

// Arbitrary number determined by empirical tests
#define GETKEYRANGE_MAX_COUNT 800

// Allows for single point of change to test other
// connection types
#define KCTL_CONTYPE	kinetic::ThreadsafeBlockingKineticConnection

enum kctl_command {
	KCTL_NOOP = 0,
	KCTL_GET,
	KCTL_GETNEXT,
	KCTL_GETPREV,
	KCTL_GETVERS,
	KCTL_RANGE,
	KCTL_PUT,
	KCTL_DEL,
	KCTL_GETLOG,
	KCTL_SETCLUSTERV,
	KCTL_SETLOCKPIN,
	KCTL_LOCK,
	KCTL_UNLOCK,
	KCTL_ACL,
	
	KCTL_EOT // End of Table -  Must be last
};

struct kargs {
	char		*ka_progname;
	kctl_command	ka_cmd;		// command, ex. info, get, put
	char		*ka_cmdstr;     // command ascii str, ex. "info", "get"
	char		*ka_key;	// key, raw unencoded buffer
	size_t		ka_keylen;	// key len, raw unencoded buffer
	char		*ka_val;	// value, raw unencoded buffer
	size_t		ka_vallen;	// value len, raw unencoded buffer
	int		ka_user;	// connection user ID 
	char		*ka_hmac;	// connection password for the user ID used
	char		*ka_host;	// connection host 
	int		ka_port;	// connection port, ex 8123 (nonTLS), 8443 (TLS)
	int		ka_usessl;	// connection boolean to use or not use TLS
	unsigned int	ka_timeout;	// connection timeout
	int64_t		ka_clustervers;	// Client cluster version number,
					// must match server cluster version number
	int		ka_quiet;	// output ctl
	int		ka_terse;	// output ctl
	int		ka_verbose;	// output ctl
	int		ka_yes;		// answer yes to any prompts
};

// Ask for user input on stdin, boolean answer. Only chars 'yYnN' and a newline
// accepted as user answer. newline accepts the default answer.
// const char *prompt; 			is the message to prompt the user with.
// unsigned int default answer;  	is the default answer
// unsigned int attempts;		is the max tries to get a valid answer
//					  if exhausted, default answer is returned.
extern int yorn(const char *, unsigned int, unsigned int);

// Dump a buffer as hex and ascii
extern void hexdump(const void*, size_t);

// Dump a buffer as a encoded string with unprintable chars as "\HH" where
// H is an ascii hex digit
extern void asciidump(const void*, size_t);

// Decode ascii arbitrary hexadecimal value escape sequences and replace with binary
// representation.  Only escape sequences /xHH are supported. A new buffer is created
// and the original buffer is copied in and escape seqs decoded. The new buffer is
// returned including the size. 
extern void * asciidecode(const void* , size_t, void**, size_t *);

#endif // _KCTL_H
