#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <inttypes.h>
#include <sys/types.h>

#include <kinetic/kinetic.h>
#include "kctl.h"

using com::seagate::kinetic::client::proto::Command_Algorithm_SHA1;
using com::seagate::kinetic::client::proto::Command_GetLog_Type_UTILIZATIONS;
using com::seagate::kinetic::client::proto::Command_GetLog_Type_TEMPERATURES;
using com::seagate::kinetic::client::proto::Command_GetLog_Type_CAPACITIES;
using com::seagate::kinetic::client::proto::Command_GetLog_Type_CONFIGURATION;
using com::seagate::kinetic::client::proto::Command_GetLog_Type_STATISTICS;
using com::seagate::kinetic::client::proto::Command_GetLog_Type_MESSAGES;
using com::seagate::kinetic::client::proto::Command_GetLog_Type_LIMITS;

using kinetic::Status;
using kinetic::KineticStatus;
using kinetic::StatusCode;
using kinetic::DriveLog;
using kinetic::Command_GetLog_Type;

using std::string;
using std::vector;
using std::shared_ptr;
using std::unique_ptr;

// Cheating with globals
static char    config, capacity, limits, msgs, ops, temps, utils, all, most;

void kctl_dump(std::unique_ptr<kinetic::DriveLog>&);

#define CMD_USAGE(_ka) kctl_info_usage(_ka)
int kctl_info_usage(struct kargs *ka) {
    fprintf(
        stderr,
        "Usage: %s [..] %s [CMD OPTIONS]\n",
        ka->ka_progname, ka->ka_cmdstr
    );

    fprintf(stderr, "\nWhere, CMD OPTIONS are [default]:\n");
    fprintf(stderr, "\t-a           Show All [default]\n");
    fprintf(stderr, "\t-c           Show Configuartion\n");
    fprintf(stderr, "\t-C           Show Capacities\n");
    fprintf(stderr, "\t-L           Show Limits\n");
    fprintf(stderr, "\t-m           Show Most [-cCLOTU] (no messages)\n");
    fprintf(stderr, "\t-M           Show Messages (can be large)\n");
    fprintf(stderr, "\t-O           Show Operation Stats\n");
    fprintf(stderr, "\t-T           Show Tempratures\n");
    fprintf(stderr, "\t-U           Show Utilizations\n");
    fprintf(stderr, "\t-?           Help\n");
    fprintf(stderr, "\nTo see available COMMON OPTIONS: ./kctl -?\n");
}

// Issue the GetLog command and dump its contents. 
int kctl_info(int argc, char *argv[], unique_ptr<KCTL_CONTYPE>& kcon, struct kargs *ka) {
    extern char *optarg;
    extern int   optind, opterr, optopt;
    char         c;

    std::unique_ptr<DriveLog>   log;
    vector<Command_GetLog_Type> types;

    KineticStatus kstatus = KineticStatus(StatusCode::OK, strerror(0));
    
    // clear global flag vars
    config = capacity = limits = msgs = ops = temps = utils = all = most = 0;
    
        while ((c = getopt(argc, argv, "acCLMmOTUh?")) != EOF) {
            switch (c) {
            case 'a':
                all = 1;
                break;

            case 'm':
                most = 1;
                break;

            case 'c':
                config = 1;
                types.push_back(Command_GetLog_Type_CONFIGURATION);
                break;

            case 'C':
                capacity = 1;
                types.push_back(Command_GetLog_Type_CAPACITIES);
                break;

            case 'L':
                limits = 1;
                types.push_back(Command_GetLog_Type_LIMITS);
                break;

            case 'M':
                msgs = 1;
                types.push_back(Command_GetLog_Type_MESSAGES);
                break;

            case 'O':
                ops = 1;
                types.push_back(Command_GetLog_Type_STATISTICS);
                break;

            case 'T':
                temps = 1;
                types.push_back(Command_GetLog_Type_TEMPERATURES);
                break;

            case 'U':
                utils = 1;
                types.push_back(Command_GetLog_Type_UTILIZATIONS);
                break;

            case 'h':
            case '?':
            default:
                CMD_USAGE(ka);
                return(-1);
            }
        }
    
    // if no flags set all, default to all
    if (!all    && !most && !config && !capacity &&
        !limits && !msgs && !ops    && !temps    && !utils)
        all = 1;

    // Because the combo flags, all and most, can be combined with
    // other flags, they take precedence over the individual types
    // requested. So clear the types and push all the types
    //
    // Keep in the following order, check 'most' then 'all'
    // 'all' takes precedence over 'most'
    if (most) {
        config = capacity = limits = ops = temps = utils = 1;

        types.clear();
        types.push_back(Command_GetLog_Type_UTILIZATIONS);
        types.push_back(Command_GetLog_Type_TEMPERATURES);
        types.push_back(Command_GetLog_Type_CAPACITIES);
        types.push_back(Command_GetLog_Type_CONFIGURATION);
        types.push_back(Command_GetLog_Type_STATISTICS);
        types.push_back(Command_GetLog_Type_LIMITS);
    }

    // no need to push all types, there is a sig that does it for me, test it
    if (all) {
        config = capacity = limits = msgs = ops = temps = utils = 1;
        types.clear();
    }
    
    // Check for the cmd key parm
    if (argc - optind) {
        fprintf(stderr, "*** Too many args\n");
        CMD_USAGE(ka);
        return(-1);
    }

    // Get the log, could do it with one sig but decided to test both sigs
    if (all) { kstatus = kcon->GetLog(log);        }
    else     { kstatus = kcon->GetLog(types, log); }
    
    if (!kstatus.ok()) {
        printf("GetLog failed: %s\n", kstatus.message().c_str());
        return(-1);
    }

    (void) kctl_dump(log);
    
    return 0;
}

void
kctl_dump(std::unique_ptr<kinetic::DriveLog>& log)
{
    if (config) {
        printf("Configuration:\n");
        printf("  Vendor:           %s\n",
               log->configuration.vendor.c_str());
        printf("  Model:            %s\n",
               log->configuration.model.c_str());
        printf("  SN:               %s\n",
               log->configuration.serial_number.c_str());
        printf("  Version:          %s\n",
               log->configuration.version.c_str());
        printf("  Compilation Date: %s\n",
               log->configuration.compilation_date.c_str());
        printf("  Source Hash:      %s\n",
               log->configuration.source_hash.c_str());
        printf("  Port:             %d\n", log->configuration.port);
        printf("  TLS Port:         %d\n", log->configuration.tls_port);
        printf("\n");
    }

    if (capacity) {
        printf("Capacity:\n");
        printf("  Total             %" PRIu64 " GiB\n",
               log->capacity.nominal_capacity_in_bytes/1024/1024/1024);
        printf("  Used              %f%% full\n",
               log->capacity.portion_full);
        printf("\n");
    }

    if (limits) {
        printf("Limits: \n");
        printf("  Max Key Size:     %d\n", log->limits.max_key_size);
        printf("  Max Value Size:   %d\n", log->limits.max_value_size);
        printf("  Max Version Size: %d\n", log->limits.max_version_size);
        printf("  Max Tag Size:     %d\n", log->limits.max_tag_size);
        printf("  Max Message Size: %d\n", log->limits.max_message_size);
        printf("  Max Connections:  %d\n", log->limits.max_connections);
        printf("  Max Read Queue:   %d\n",
               log->limits.max_outstanding_read_requests);
        printf("  Max Write Queue:  %d\n",
               log->limits.max_outstanding_write_requests);
        printf("\n");
    }

    if (utils) {
        printf("Utilizations:\n");
        for (auto it = log->utilizations.begin();
             it != log->utilizations.end();
             ++it) {
            char s[20];
            strcpy(s, it->name.c_str()); strcat(s,":");
            printf("  %-17s %.02f%%\n", s, it->percent);
        }
        printf("\n");
    }


    if (temps) {
        printf("Temperatures:\n");
        for (auto it = log->temperatures.begin();
             it != log->temperatures.end();
             ++it) {
            char s[20];
            strcpy(s, it->name.c_str()); strcat(s,":");
            printf("  %-17s %.0f\u00B0C\n", s, it->current_degc);
        }
        printf("\n");
    }

    if (ops) {
        printf(" Operation Statistics:\n");
        printf("  %24s  %8s %8s\n", "Message Type", "Issued", "Bytes");
        for (auto it = log->operation_statistics.begin();
             it != log->operation_statistics.end();
             ++it) {
            printf("  %24s: %8" PRId64 " %8" PRId64 "\n",
                   it->name.c_str(), it->count, it->bytes);
        }
        printf("\n");
    }

    if (msgs) {
        printf("Messages:\n");
        printf("%s\n", log->messages.c_str());
        printf("\n");
    }
}

