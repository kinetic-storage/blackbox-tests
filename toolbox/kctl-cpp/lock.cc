#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>

#include <glog/logging.h>
#include <kinetic/kinetic.h>
#include "kctl.h"
#include "gflags/gflags.h"

using com::seagate::kinetic::client::proto::Command_Algorithm_SHA1;
using kinetic::Status;
using kinetic::KineticStatus;
using kinetic::KineticRecord;
using kinetic::StatusCode;

using std::shared_ptr;
using std::string;
using std::unique_ptr;

#define CMD_USAGE(_ka) kctl_lock_usage(_ka)
int
kctl_lock_usage(struct kargs *ka)
{
        fprintf(stderr, "Usage: %s [..] %s [CMD OPTIONS] -p pin %s\n",
		ka->ka_progname, ka->ka_cmdstr,
		(ka->ka_cmd == KCTL_SETLOCKPIN)?"-n pin":"");
	fprintf(stderr, "\nWhere, CMD OPTIONS are [default]:\n");
	fprintf(stderr, "\t-p pin       Current PIN\n");
	if (ka->ka_cmd == KCTL_SETLOCKPIN)
		fprintf(stderr, "\t-n pin       New PIN\n");
	fprintf(stderr, "\t-?           Help\n");
	fprintf(stderr, "\nTo see available COMMON OPTIONS: ./kctl -?\n");
}

// Set the cluster version at the kinetic server
int
kctl_lock(int argc, char *argv[],
	 unique_ptr<KCTL_CONTYPE>& kcon,
	 struct kargs *ka)
{
 	extern char     	*optarg;
        extern int		optind, opterr, optopt;
        char			c, *cp, *pin, *npin;
	int64_t			cversion = -1;

	pin = npin = NULL; // Initialize
	
        while ((c = getopt(argc, argv, "p:n:h?")) != EOF) {
                switch (c) {
		case 'p':
			pin = optarg;
			break;
 		case 'n':
			npin = optarg;
			break;
		case 'h':
		case '?':
                default:
                        CMD_USAGE(ka);
			return(-1);
		}
        }

	// pin is required, is it set?
	if (!pin) {
		fprintf(stderr, "*** -p pin is required\n");
		CMD_USAGE(ka);
		return(-1);
	}

	if (ka->ka_cmd == KCTL_SETLOCKPIN) {
		// npin is required, is it set?
		if (!npin) {
			fprintf(stderr, "*** -n pin is required\n");
			CMD_USAGE(ka);
			return(-1);
		}
	}
	
	// Check for erroneous params
	if (argc - optind > 0) {
		fprintf(stderr, "*** Too many args\n");
		CMD_USAGE(ka);
		return(-1);
	}

	// Three different commands handled here: setpin, lock and unlock.
	KineticStatus	kstatus = KineticStatus(StatusCode::OK, strerror(0));
	if (ka->ka_cmd == KCTL_SETLOCKPIN)
		kstatus = kcon->SetLockPIN(npin, pin);
	else if	(ka->ka_cmd == KCTL_LOCK)
		kstatus = kcon->LockDevice(pin);
	else if	(ka->ka_cmd == KCTL_UNLOCK)
		kstatus = kcon->UnlockDevice(pin);

	if(!kstatus.ok()) {
		fprintf(stderr, "%s: Unable to lock kinetic device: %s\n",
			ka->ka_cmdstr, kstatus.message().c_str());
		return(-1);
	}		

	if (ka->ka_verbose) {
		if (ka->ka_cmd == KCTL_SETLOCKPIN)
			printf("New PIN set\n");
		else if (ka->ka_cmd == KCTL_LOCK)
			printf("Device is LOCKED\n");
		else if (ka->ka_cmd == KCTL_UNLOCK)
			printf("Device is UNLOCKED\n");
		else
			printf("This is not my beautiful wife...\n");
	}
	return(0);
}
