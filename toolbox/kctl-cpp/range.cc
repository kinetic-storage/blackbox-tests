#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>

#include <glog/logging.h>
#include <kinetic/kinetic.h>
#include "kctl.h"
#include "gflags/gflags.h"

using com::seagate::kinetic::client::proto::Command_Algorithm_SHA1;
using com::seagate::kinetic::client::proto::Command_GetLog_Type_LIMITS;

using kinetic::KineticConnectionFactory;
using kinetic::Status;
using kinetic::StatusCode;
using kinetic::KineticStatus;
using kinetic::KineticRecord;
using kinetic::KeyRangeIterator;
using kinetic::DriveLog;
using kinetic::Command_GetLog_Type;

using std::string;
using std::vector;
using std::shared_ptr;
using std::unique_ptr;

#define CMD_USAGE(_ka) kctl_range_usage(_ka)

int
kctl_range_usage(struct kargs *ka)
{
        fprintf(stderr, "Usage: %s [..] %s [CMD OPTIONS]\n",
		ka->ka_progname, ka->ka_cmdstr);
	fprintf(stderr, "\nWhere, CMD OPTIONS are [default]:\n");
	fprintf(stderr, "\t-n count	Number of keys [unlimited]\n");       
	fprintf(stderr, "\t-s KEY       Start Key in the range, non inclusive\n");
	fprintf(stderr, "\t-S KEY       Start Key in the range, inclusive\n");
	fprintf(stderr, "\t-e KEY       End Key in the range, non inclusive\n");
	fprintf(stderr, "\t-E KEY       End Key in the range, inclusive\n");
	fprintf(stderr, "\t-A           Show keys as modified ascii string\n");
	fprintf(stderr, "\t-X           Show keys as hex and ascii\n");
	//	fprintf(stderr, "\t-r           Reverse the order\n");
	fprintf(stderr, "\t-?           Help\n");

	// R"foo(....)foo" is a non escape char evaluated string literal 
	fprintf(stderr, "\nWhere, KEY is a quoted string that can contain arbitrary\n");
	fprintf(stderr, "hexidecimal escape sequences to encode binary characters.\n");
	fprintf(stderr, R"foo(Only \xHH escape sequences are converted, ex \xF8.)foo");
	fprintf(stderr, "\nIf a conversion fails the command terminates.\n");

	fprintf(stderr, "\nTo see available COMMON OPTIONS: ./kctl -?\n");
}

// Set the cluster version at the kinetic server
int
kctl_range(int argc, char *argv[],
	 unique_ptr<KCTL_CONTYPE>& kcon,
	 struct kargs *ka)
{
 	extern char     *optarg;
        extern int	optind, opterr, optopt;
        char		c, *cp;
	char 		start = 0, starti = 0, end = 0, endi = 0;
	string 		startk = "";   // Empty start in case none 
	string 		endk = "";     // end key
        int 		count = -1;
	int		adump = 0, hdump = 0;
	KineticStatus 	kstatus = KineticStatus(StatusCode::OK, strerror(0));
	std::unique_ptr<DriveLog>		log;
	vector<Command_GetLog_Type>		types;
	unique_ptr<kinetic::KineticRecord>	record;	
        while ((c = getopt(argc, argv, "s:S:e:E:n:AXh?")) != EOF) {
                switch (c) {
		case 'n':
			count = strtol(optarg, &cp, 0);
			if (!cp || *cp != '\0') {
				fprintf(stderr, "*** Invalid count %s\n",
				       optarg);
				CMD_USAGE(ka);
				return(-1);
			}
			break;
		case 's':
			if (starti) {
				fprintf(stderr, "only one of -[sS]\n");
				CMD_USAGE(ka);
				return(-1);
			}
			start = 1;
			startk = optarg;
			break;
		case 'S':
			if (start) {
				fprintf(stderr, "only one of -[sS]\n");
				CMD_USAGE(ka);
				return(-1);
			}
			starti = 1;
			startk = optarg;
			break;
		case 'e':
			if (end) {
				fprintf(stderr, "only one of -[eE]\n");
				CMD_USAGE(ka);
				return(-1);
			}
			endk = optarg;
		        end = 1;
			break;
		case 'E':
			if (endi) {
				fprintf(stderr, "only one of -[eE]\n");
				CMD_USAGE(ka);
				return(-1);
			}
			endk = optarg;
			endi = 1;
			break;
		case 'A':
			adump = 1;
			if (hdump) {
				fprintf(stderr, "**** -X and -A are exclusive\n");
				CMD_USAGE(ka);
				return(-1);
			}
			break;
		case 'X':
			hdump = 1;
			if (adump) {
				fprintf(stderr, "**** -X and -A are exclusive\n");
				CMD_USAGE(ka);
				return(-1);
			}
			break;
		case 'h':
                case '?':
                default:
                        CMD_USAGE(ka);
			return(-1);
		}
        }

	// Check for erroneous params
	if (argc - optind > 0) {
		fprintf(stderr, "*** Too many args\n");
		CMD_USAGE(ka);
		return(-1);
	}

	// Setup the range
	// If no start key provided, nothing to do as the empty string
	// would act as the first possible key
	//
	// if no end key provided use the last possible key which is a string of
	// all FFs. The size of that string should be equal to the max key length.
	// Get max key size from GetLog(LIMITS).
	if (!endk.size()) {
		types.push_back(Command_GetLog_Type_LIMITS);
		kstatus = kcon->GetLog(types, log);
		if(!kstatus.ok()) {
			printf("Get limits failed: %s\n",
			       kstatus.message().c_str());
			return(-1);
		}

		// No end given, use all FFs -- is the last possible key
		for (int i = 0; i < log->limits.max_key_size; i++) {
			endk += "\xFF";
		}
	}

	kinetic::KeyRangeIterator krit = KeyRangeIterator();
	std::unique_ptr<vector<string>> keys;
	
	// If verbose dump the range we are acting on
	// Print first 5 chars of each key defining the range
	if (ka->ka_verbose)  {
		printf("Keys [");
		if (!start && !starti )
			printf("{START}");
		else
			if (adump)
				asciidump(startk.substr(0,5).c_str(), 5);
			else
				printf("%s", startk.substr(0,5).c_str());
		
		printf(":");
		
		if (!end && !endi )
			printf("{END}");
		else
			if (adump)
				asciidump(endk.substr(0,5).c_str(),5);
			else
				printf("%s", endk.substr(0,5).c_str());
		printf(":");

		
		if (count > 0)
			printf("%u]\n", count);
		else
			printf("unlimited]\n");
	}

	// Iterate over all the keys and print them out

	// If 0 < count <= GETKEYRANGE_MAX_COUNT (count=-1 is unlimited)
	// then use a single GetKeyRange call, no need to iterate.
	// Of course this is unnecessary but allows the caller to test
	// GetKeyRange directly without going through the IterateKeyRange code.
	if ((count > 0) && (count <= GETKEYRANGE_MAX_COUNT)) {
		kstatus = kcon->GetKeyRange(startk, starti, endk, endi, 0, count, keys);
		if(!kstatus.ok()) {
			fprintf(stderr,					\
				"%s: Unable to get key range: %s\n",
				ka->ka_cmdstr, kstatus.message().c_str());
			return(-1);
		}

		if (ka->ka_verbose) printf("GKR\n");
		
		for(int i=0; i<keys->size(); i++) {
			if (ka->ka_verbose)
				printf("%u: ", i);
			if (hdump)
				hexdump(keys->at(i).c_str(), keys->at(i).length());
			else if (adump)
			        asciidump(keys->at(i).c_str(),
					  keys->at(i).length()), printf("\n");
			else
				printf("%s\n", keys->at(i).c_str());
		}

		// Success so return
		return(0);
	}

	// Keys requested are larger than a single GetKeyRange call
	// can return, so use IterateKeyRange. Set framesize to
	// GETKEYRANGE_MAX_COUNT to maximize network efficiency,
	// making as few server calls as possible.
	// the IterateKeyRange call throws failures when they occur
	// not catching them means more error checking of the
	// returned data so lets catch them and exit quickly.
	if (ka->ka_verbose) printf("IKR\n");
	try {
		// Init the kinetic range iterator
		unsigned int framesz = GETKEYRANGE_MAX_COUNT, i=1;

		krit = kcon->IterateKeyRange(startk, starti, endk, endi,framesz);
		while (krit != kinetic::KeyRangeEnd() && count) {
			if (ka->ka_verbose)
				printf("%u: ", i++);

			// Dump the keys as requested
			if (hdump)
				hexdump(krit->c_str(),krit->length());
			else if (adump)
				asciidump(krit->c_str(),krit->length()), printf("\n");
			else
				printf("%s\n", krit->c_str());		

			++krit; // Advance the iter

				// count == -1; means unlimited count
			if (count < 0 )
				continue;

			count--;
		}
	} catch (std::runtime_error &e) {
		printf("Iterator Failed: %s\n", e.what());
		return(-1);
	}
	
	return(0);
}

