#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>

#include <glog/logging.h>
#include <kinetic/kinetic.h>
#include "kctl.h"
#include "gflags/gflags.h"

using com::seagate::kinetic::client::proto::Command_Algorithm_SHA1;
using kinetic::Status;
using kinetic::KineticStatus;
using kinetic::KineticRecord;
using kinetic::StatusCode;

using std::shared_ptr;
using std::string;
using std::unique_ptr;

#define CMD_USAGE(_ka) kctl_get_usage(_ka)
int
kctl_get_usage(struct kargs *ka)
{
        fprintf(stderr, "Usage: %s [..] %s [CMD OPTIONS] KEY\n",
        ka->ka_progname, ka->ka_cmdstr);
    fprintf(stderr, "\nWhere, CMD OPTIONS are [default]:\n");
    fprintf(stderr, "\t-A           Dumps key/value as ascii w/escape seqs\n");
    fprintf(stderr, "\t-X           Dumps key/value as both hex and ascii\n");
    fprintf(stderr, "\t-?           Help\n");

    fprintf(stderr, "\nWhere, KEY is a quoted string that can contain arbitrary\n");
    fprintf(stderr, "hexidecimal escape sequences to encode binary characters.\n");
    fprintf(stderr, R"foo(Only \xHH escape sequences are converted, ex \xF8.)foo");
    fprintf(stderr, "\nIf a conversion fails the command terminates.\n");
    
    fprintf(stderr,    "\nBy default keys and values are printed as raw strings,\n");
    fprintf(stderr,    "including special/nonprintable chars\n");
    fprintf(stderr, "\nTo see available COMMON OPTIONS: ./kctl -?\n");
}

int kctl_get(int argc, char *argv[], unique_ptr<KCTL_CONTYPE>& kcon, struct kargs *ka) {
    extern char *optarg;
    extern int   optind, opterr, optopt;
    char         c;
    int          hdump = 0, adump = 0;

    std::unique_ptr<KineticRecord> record;
    std::unique_ptr<string>        nextkey;
    std::unique_ptr<string>        version;

    KineticStatus kstatus = KineticStatus(StatusCode::OK, strerror(0));
    
    while ((c = getopt(argc, argv, "AXh?")) != EOF) {
    	switch (c) {
        	case 'A':
        	    adump = 1;
        	    if (hdump) {
        	        fprintf(stderr, "*** -X and -A are exclusive\n");
        	        CMD_USAGE(ka);
        	        return(-1);
        	    }
        	    break;

        	case 'X':
        	    hdump = 1;
        	    if (adump) {
        	        fprintf(stderr, "*** -X and -A are exclusive\n");
        	        CMD_USAGE(ka);
        	        return(-1);
        	    }
        	    break;

        	case 'h':
        	        case '?':
        	        default:
        	                CMD_USAGE(ka);
        	    return(-1);
        }
    }

    // Check for the cmd key parm
    if (argc - optind == 1) {
        // Aways decode any ascii arbitrary hexadecimal value escape
        // sequences in the passed-in key, if none present this amounts to
        // a copy.
        if (!asciidecode(argv[optind], strlen(argv[optind]),
                 (void **)&ka->ka_key, &ka->ka_keylen)) {
            fprintf(stderr, "*** Failed key conversion\n");
            CMD_USAGE(ka);
            return(-1);
        }
#if 0
        printf("%s\n", argv[optind]);
        printf("%lu\n", ka->ka_keylen);
        hexdump(ka->ka_key, ka->ka_keylen);
#endif 
    } else {
        fprintf(stderr, "*** Too few or too many args\n");
        CMD_USAGE(ka);
        return(-1);
    }

    // Since the key could have nulls in it a simple char * str won't work
    std::string key(ka->ka_key, ka->ka_keylen);

    // 4 cmd supported here: Get, GetNext, GetPrev, GetVers
    if (ka->ka_cmd == KCTL_GET)
        kstatus = kcon->Get(key, record);
    else if    (ka->ka_cmd == KCTL_GETNEXT)
        kstatus = kcon->GetNext(key, nextkey, record);
    else if    (ka->ka_cmd == KCTL_GETPREV)
        kstatus = kcon->GetPrevious(key, nextkey, record);
    else if    (ka->ka_cmd == KCTL_GETVERS)
        kstatus = kcon->GetVersion(key, version);
    else {
        fprintf(stderr, "Bad command: %s\n", ka->ka_cmdstr);
        return(-1);
    }
        
    if(!kstatus.ok()) {
        printf("%s failed: %s\n", ka->ka_cmdstr, kstatus.message().c_str());
        return(-1);
    }

    printf("Key(");
    if (adump) {
        asciidump(key.c_str(), key.length());
    } else if (hdump) {
        printf("\n");
        hexdump(key.c_str(), key.length());
    } else {
        printf("%s", ka->ka_key);
    }
    printf("): ");
    
    if (ka->ka_cmd == KCTL_GETVERS) {
        printf("%s\n", version->c_str());
        return(0);
    }

    printf("\nLength: %lu\n", record->value()->length());
    if (adump) {
        asciidump(record->value()->c_str(),record->value()->length());
    } else if (hdump) {
        hexdump(record->value()->c_str(),record->value()->length());
    } else {
        printf("%s", record->value()->c_str());
    }
    printf("\n");
    return(0);
}

